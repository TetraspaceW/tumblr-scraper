import requests
import json
import os
from dotenv import load_dotenv
import logging

def getTumblr(pageNumber, blog):
    logging.debug(f"Retrieving page {pageNumber} for blog {blog}")
    # get TUMBLR_AUTHORISATION and TUMBLR_COOKIE from the /posts request when viewing the blog in the dashboard and create a .env file

    authorisation, cookie = os.getenv("TUMBLR_AUTHORISATION"), os.getenv("TUMBLR_COOKIE")
    if not (authorisation and cookie):
        raise Exception('TUMBLR_AUTHORISATION or TUMBLR_COOKIE not set in .env file')

    URL = f"https://www.tumblr.com/api/v2/blog/{blog}/posts?fields[blogs]=name,avatar,title,url,blog_view_url,is_adult,?is_member,description_npf,uuid,can_be_followed,?followed,?advertiser_name,theme,?primary,?is_paywall_on,?paywall_access,?subscription_plan,tumblrmart_accessories,?live_now,can_show_badges,share_likes,share_following,can_subscribe,subscribed,ask,?can_submit,?is_blocked_from_primary,?is_blogless_advertiser,?tweet,is_password_protected,?admin,can_message,ask_page_title,?analytics_url,?top_tags,?allow_search_indexing,is_hidden_from_blog_network,?should_show_tip,?should_show_gift,?should_show_tumblrmart_gift,?can_add_tip_message&npf=true&reblog_info=true&include_pinned_posts=true&tumblelog={blog}&page_number={pageNumber}"
    headers = {
        "Authorization": os.getenv("TUMBLR_AUTHORISATION"),
        "Cookie": os.getenv("TUMBLR_COOKIE"),
    }

    page = requests.get(
        URL,
        headers=headers,
    )
    if page.status_code == 401:
        raise Exception(f"Unauthorized; check that the authorisation token is correct. Status code: {page.status_code}")
    elif page.status_code != 200:
        raise Exception(f"Unexpected status code: {page.status_code}.")

    res = json.loads(page.text)
    response = res["response"]

    if "posts" in response:
        posts = [post["content"] for post in response["posts"]]
    else:
        raise Exception(f"Response returned ({response}) does not include posts")

    if "_links" in response:
        nextPage = response["_links"]["next"]["href"].split("page_number=")[-1]
    else:
        nextPage = None

    return (posts, nextPage)


load_dotenv()
logging.basicConfig(level=logging.DEBUG, format='%(asctime)s - %(levelname)s - %(message)s')

# write the blog you want to scrape
blogName = ""

# copy this page from the /posts request made by the Tumblr website
page = ""

allPosts = []
try:
    logging.info(f"Started retrieving data for blog {blogName}!")
    while page:
        posts, nextPage = getTumblr(page, blogName)
        logging.debug(f"Retrieved posts {posts}")
        allPosts.extend(posts)
        page = nextPage
    logging.info(f"Finished retrieving data for blog {blogName}!")

    with open("out.json", "w") as file:
        json.dump(allPosts, file, indent=4)

except Exception as e:
    logging.error(e)
